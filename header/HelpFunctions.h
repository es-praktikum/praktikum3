#ifndef HELP_H
#define HELP_H

#include "Schedule.h"
#include "ext_headers.h"

void printSchedule(std::map<Task*, Schedule*>& ts);
void resetSchedule(std::map<Task*, Schedule*>& ts);
void generateGantt(std::map<Task*, Schedule*> ts, const char* filename);

void updateTaskQueue(std::list<Task*>& TQ, const std::list<Task*>& scenario, unsigned int c);
#endif  // HELP_H
