#ifndef MAIN_H
#define MAIN_H

#include "HelpFunctions.h"
#include "Schedule.h"
#include "Task.h"
#include "ext_headers.h"
#include "scenario.h"
#include "schedule_algorithms.h"

extern const unsigned int SIMULATION_TIME;
void computeResponseTimes(std::map<Task*, Schedule*> ts);
void computeWaitingTimes(std::map<Task*, Schedule*> ts);

#endif  // MAIN_H
