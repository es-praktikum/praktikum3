#ifndef _SCHEDULER_ALGO_H
#define _SCHEDULER_ALGO_H

#include "HelpFunctions.h"
#include "MainProgram.h"
#include "Schedule.h"
#include "Task.h"
#include "ext_headers.h"

/** FCFS **/
bool sortByReleaseTime(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> scheduleFCFS(std::list<Task*>& scenario);

/** SJF **/
bool sortByExecutionTime(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> scheduleSJF(std::list<Task*>& scenario);

/** SRTN **/
bool sortByRemainingExecutionTime(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> scheduleSRTN(std::list<Task*>& scenario);

/** PB **/
bool sortByPriority(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> schedulePB(std::list<Task*>& scenario);

/** RR **/
bool sortByName(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> scheduleRR(std::list<Task*>& scenario, unsigned int timeQuantum);

/** EDD **/
bool sortByDeadline(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> scheduleEDD(std::list<Task*>& scenario);

/** EDF **/
std::map<Task*, Schedule*> scheduleEDF(std::list<Task*>& scenario);

/** EDF_STAR**/
bool topSortForward(const Task* Ta, const Task* Tb);
bool topSortBackward(const Task* Ta, const Task* Tb);
std::map<Task*, Schedule*> scheduleEDFstar(std::list<Task*>& scenario);

/** DM **/
bool sortByRelativeDeadline(const Task* Tl, const Task* Tr);
std::map<Task*, Schedule*> scheduleDM(std::list<Task*>& scenario);

#endif  //_SCHEDULER_ALGO_H
