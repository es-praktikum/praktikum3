#ifndef _SCENARIOS_H
#define _SCENARIOS_H

#include "Schedule.h"
#include "Task.h"
#include "ext_headers.h"

std::list<Task*> createScenario1();
std::list<Task*> createScenario2();
std::list<Task*> createScenario3();
std::list<Task*> createScenario4();
std::list<Task*> createScenario5();
std::list<Task*> createScenario6();
std::list<Task*> createScenario7();

#endif  //_SCENARIOS_H
