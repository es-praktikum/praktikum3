PROG   = MainProgram

GCC    = g++
CFLAGS = -O0 -Wall -g
DFLAGS =
.PHONY:FCFS SJF SRTN PB RR EDD EDF1 EDF2 EDF_STAR EDF3 DM

SCHEDULER = src/scheduler/scheduleDM.cc \
	src/scheduler/scheduleEDD.cc \
	src/scheduler/scheduleEDF.cc \
	src/scheduler/scheduleEDFstar.cc \
	src/scheduler/scheduleFCFS.cc \
	src/scheduler/schedulePB.cc \
	src/scheduler/scheduleRR.cc \
	src/scheduler/scheduleSJF.cc \
	src/scheduler/scheduleSRTN.cc
SCENARIO = src/scenarios/scenario1.cc \
	src/scenarios/scenario2.cc \
	src/scenarios/scenario3.cc \
	src/scenarios/scenario4.cc \
	src/scenarios/scenario5.cc \
	src/scenarios/scenario6.cc \
	src/scenarios/scenario7.cc 

SRC = src/Schedule.cc \
      src/Task.cc \
      src/MainProgram.cc  \
      src/HelpFunctions.cc



OBJ = $(SRC:.cc=.o)
OBJ_SCHEDULER = $(SCHEDULER:.cc=.o)
OBJ_SCENARIO = $(SCENARIO:.cc=.o)

PROG = MainProgram

INCLUDE = ./header/

all: $(SRC) $(PROG)

runall: FCFS SJF SRTN PB RR EDD EDF1 EDF2 EDF_STAR EDF3 EDF4 DM

$(PROG): $(OBJ_SCHEDULER) $(OBJ_SCENARIO) $(OBJ)
	$(GCC) ${OBJ} $(OBJ_SCHEDULER) $(OBJ_SCENARIO) ${CFLAGS} -o $@

%.o: %.cc
	$(GCC) -I${INCLUDE} ${CFLAGS} ${DFLAGS} -c -o $@ $<

FCFS: all
	./$(PROG) FCFS
	./showGanttChart schedule_FCFS.txt &

SJF: all
	./$(PROG) SJF
	./showGanttChart schedule_SJF.txt &

SRTN: all
	./$(PROG) SRTN
	./showGanttChart schedule_SRTN.txt &
PB: all
	./$(PROG) PB
	./showGanttChart schedule_PB.txt &
RR: all
	./$(PROG) RR
	./showGanttChart schedule_RR1.txt &
	./showGanttChart schedule_RR2.txt &
	./showGanttChart schedule_RR3.txt &
	./showGanttChart schedule_RR5.txt &
EDD: all
	./$(PROG) EDD
	./showGanttChart schedule_EDD.txt &
EDF1: all
	./$(PROG) EDF1
	./showGanttChart schedule_EDF1.txt &
EDF2: all
	./$(PROG) EDF2
	./showGanttChart schedule_EDF2.txt &
EDF3: all
	./$(PROG) EDF3
	./showGanttChart schedule_EDF3.txt &
EDF_STAR: all
	./$(PROG) EDF_STAR
	./showGanttChart schedule_EDF_STAR.txt &
EDF4: all
	./$(PROG) EDF4
	./showGanttChart schedule_EDF4.txt &
DM: all
	./$(PROG) DM
	./showGanttChart schedule_DM.txt &

clean:
	
	rm -f 
	rm -f src/*.o  src/scheduler/*.o src/scenarios/*.o ${PROG} core schedule_*.txt




