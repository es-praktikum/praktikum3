/**
 *******************************************************************************
 * \file MainProgram.cc
 *
 * \brief Test program.
 *
 * \attention   Copyright (C) 2009-2010 by University of Erlangen-Nuremberg,
 *              Department of Hardware-Software-Co-Design, Germany.
 *              All rights reserved.
 *
 * \author Frank Hannig
 *
 * $Id:  $
 *
 *******************************************************************************
 */

#include "MainProgram.h"

const unsigned int SIMULATION_TIME = 81;

using namespace std;
/** *******************************************************************
 * Computation of response times
 * ********************************************************************
 */
void computeResponseTimes(map<Task*, Schedule*> ts) {
    for (map<Task*, Schedule*>::const_iterator it = ts.begin(); it != ts.end(); ++it) {
        Task* T = it->first;
        Schedule* S = it->second;

        // Calculate response time ta of task T
        // ...
        cout << "The response time of task " << T->getName() << " is "
             << "???" << endl;
    }
    cout << "The average response time is "
         << "???" << endl;
}

/** *******************************************************************
 * Computation of waiting times
 * ********************************************************************
 */
void computeWaitingTimes(map<Task*, Schedule*> ts) {
    for (map<Task*, Schedule*>::const_iterator it = ts.begin(); it != ts.end(); ++it) {
        Task* T = it->first;
        Schedule* S = it->second;

        // Calculate waiting time tw of task T
        cout << "The waiting time of task " << T->getName() << " is "
             << "???" << endl;
    }
    cout << "The average waiting time is "
         << "???" << endl;
}

void Part1() {
    cout << "\n\n\33[1m***********************************************************************\n";
    cout << "***                                                                 ***\n";
    cout << "*** P A R T   I                                                     ***\n";
    cout << "***                                                                 ***\n";
    cout << "*** Task scheduling \33[1;4mwithout\33[0m\33[1m deadlines                               ***\n";
    cout << "***                                                                 ***\n";
    cout << "***********************************************************************\33[0m\n";
}
void FCFS() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: First Come First Served (FCFS)             ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario1();

    map<Task*, Schedule*> taskSchedules = scheduleFCFS(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_FCFS.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void SJF() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Shortest Job First (SJF)                   ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario1();

    map<Task*, Schedule*> taskSchedules = scheduleSJF(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_SJF.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void SRTN() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Shortest Remaining Time Next (SRTN)        ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario1();

    map<Task*, Schedule*> taskSchedules = scheduleSRTN(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_SRTN.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void PB() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Priority-based (PB)                        ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario1();

    map<Task*, Schedule*> taskSchedules = schedulePB(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_PB.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void RR() {
    list<Task*> scenario = createScenario1();
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Round-Robin (RR) with time quantum 5       ***\n";
    cout << "***********************************************************************\n";
    map<Task*, Schedule*> taskSchedules = scheduleRR(scenario, 5);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_RR5.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Round-Robin (RR) with time quantum 3       ***\n";
    cout << "***********************************************************************\n";

    scenario = createScenario1();
    taskSchedules = scheduleRR(scenario, 3);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_RR3.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Round-Robin (RR) with time quantum 2       ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario1();
    taskSchedules = scheduleRR(scenario, 2);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_RR2.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Round-Robin (RR) with time quantum 1       ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario1();
    taskSchedules = scheduleRR(scenario, 1);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_RR1.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void PART2() {
    cout << "\n\n\33[1m***********************************************************************\n";
    cout << "***                                                                 ***\n";
    cout << "*** P A R T   I I                                                   ***\n";
    cout << "***                                                                 ***\n";
    cout << "*** Task scheduling \33[1;4mwith\33[0m\33[1m deadlines                                  ***\n";
    cout << "***                                                                 ***\n";
    cout << "***********************************************************************\33[0m\n";
}

void EDD() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Earliest Due Date (EDD)                    ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario2();

    map<Task*, Schedule*> taskSchedules = scheduleEDD(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_EDD.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void EDF1() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Earliest Deadline First (EDF)              ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario3();

    map<Task*, Schedule*> taskSchedules = scheduleEDF(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_EDF1.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void EDF2() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Earliest Deadline First (EDF)              ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario4();

    map<Task*, Schedule*> taskSchedules = scheduleEDF(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_EDF2.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void EDF3() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Earliest Deadline First (EDF)              ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario5();
    map<Task*, Schedule*> taskSchedules = scheduleEDF(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_EDF3.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void EDF_STAR() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Earliest Deadline First Star (EDF*)        ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario5();

    map<Task*, Schedule*> taskSchedules = scheduleEDFstar(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_EDF_STAR.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void PART3() {
    cout << "\n\n\33[1m***********************************************************************\n";
    cout << "***                                                                 ***\n";
    cout << "*** P A R T   I I I                                                 ***\n";
    cout << "***                                                                 ***\n";
    cout << "*** Scheduling of \33[1;4mperiodic\33[0m\33[1m tasks                                    ***\n";
    cout << "***                                                                 ***\n";
    cout << "***********************************************************************\33[0m\n";
}

void EDF4() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Earliest Deadline First (EDF)              ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario6();

    map<Task*, Schedule*> taskSchedules = scheduleEDF(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_EDF4.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

void DM() {
    list<Task*> scenario;
    string input;
    cout << "\n\n***********************************************************************\n";
    cout << "*** Scheduling strategy: Deadline-Monotonic (DM)                    ***\n";
    cout << "***********************************************************************\n";
    scenario = createScenario7();

    map<Task*, Schedule*> taskSchedules = scheduleDM(scenario);
    printSchedule(taskSchedules);
    generateGantt(taskSchedules, "schedule_DM.txt");
    computeResponseTimes(taskSchedules);
    computeWaitingTimes(taskSchedules);

    cout << "\nPress <return> to continue\n";
    getline(cin, input);
}

/** *******************************************************************
 *
 *  M A I N   P R O G R A M
 *
 * ********************************************************************
 */
int main(int argc, const char** argv) {
    if (argc != 2) {
        cout << "Wrong Usage: MainProgram {algorithm name}";
        exit(-1);
    }

    if (argv[1] == string("FCFS") || argv[1] == string("SJF") || argv[1] == string("SRTN") || argv[1] == string("PB") ||
        argv[1] == string("RR")) {
        Part1();
    }

    if (argv[1] == string("EDD") || argv[1] == string("EDF1") || argv[1] == string("EDF2") ||
        argv[1] == string("EDF3") || argv[1] == string("EDF_STAR")) {
        PART2();
    }

    if (argv[1] == string("EDF4") || argv[1] == string("DM")) {
        PART3();
    }
    if (argv[1] == string("FCFS")) FCFS();
    if (argv[1] == string("SJF")) SJF();
    if (argv[1] == string("SRTN")) SRTN();
    if (argv[1] == string("PB")) PB();
    if (argv[1] == string("RR")) RR();

    if (argv[1] == string("EDD")) EDD();
    if (argv[1] == string("EDF1")) EDF1();
    if (argv[1] == string("EDF2")) EDF2();
    if (argv[1] == string("EDF3")) EDF3();
    if (argv[1] == string("EDF_STAR")) EDF_STAR();
    if (argv[1] == string("EDF4")) EDF4();
    if (argv[1] == string("DM")) DM();

    return 0;
}
