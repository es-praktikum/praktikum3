#include "schedule_algorithms.h"
using namespace std;

bool sortByDeadline(const Task* Tl, const Task* Tr) {
    return (true);  // Change this line!
}

map<Task*, Schedule*> scheduleEDD(
    list<Task*>& scenario) {  // ******************************************************************
    // * Create for all tasks in scenario an empty schedule
    // ******************************************************************
    map<Task*, Schedule*> taskSchedules;
    for (list<Task*>::const_iterator it = scenario.begin(); it != scenario.end(); ++it) {
        Task* T = *it;
        Schedule* S = new Schedule(T, SIMULATION_TIME);
        taskSchedules[T] = S;
    }

    // ******************************************************************
    // * Create an empty task queue
    // ******************************************************************
    list<Task*> TQ;

    // ******************************************************************
    // * Main simulation loop
    // ******************************************************************
    Task* T = NULL;
    list<Task*>::iterator it;
    for (unsigned int cycle = 0; cycle < SIMULATION_TIME;
         cycle++) {  // Update the task queue since new tasks might have been released
        updateTaskQueue(TQ, scenario, cycle);
        // Sort tasks according to their release times
        TQ.sort(sortByDeadline);

        // ****************************************************************
        // * Implementation of EDD scheduler
        // ****************************************************************

        // Only if there are tasks in the queue or an active task,
        // we have something to do
        if ((!TQ.empty()) || (T != NULL)) {
            if (T == NULL) {  // If there is no active task, dispatch the first task from the queue
                it = TQ.begin();
                T = *it;
            }

            // Schedule task in this cycle
            Schedule* S = taskSchedules[T];
            S->active(cycle);

            // Get remaining execution time of task T
            unsigned int d = T->getRemainingExecutionTime();
            if (d == 1) {  // The task has been totaly executed and can be removed from the task queue
                TQ.erase(it);
                T = NULL;
            } else {  // Decrement the remaining execution time by one cycle
                T->setRemainingExecutionTime(d - 1);
            }
        }
    }
    return taskSchedules;
}
