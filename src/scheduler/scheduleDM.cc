#include "schedule_algorithms.h"
using namespace std;

bool sortByRelativeDeadline(const Task* Tl, const Task* Tr) {
    return (Tl->getBasicDeadline() < Tr->getBasicDeadline());
}
map<Task*, Schedule*> scheduleDM(
    list<Task*>& scenario) {  // ******************************************************************
    // * Create for all tasks in scenario an empty schedule
    // ******************************************************************
    map<Task*, Schedule*> taskSchedules;

    for (list<Task*>::const_iterator it = scenario.begin(); it != scenario.end(); ++it) {
        Task* T = *it;
        Schedule* S = new Schedule(T, SIMULATION_TIME);
        taskSchedules[T] = S;
    }

    // ******************************************************************
    // * Create an empty task queue
    // ******************************************************************
    list<Task*> TQ;

    // ******************************************************************
    // * Main simulation loop
    // ******************************************************************
    for (unsigned int cycle = 0; cycle < SIMULATION_TIME; cycle++) {
        // Test on missed Deadlines
        for (list<Task*>::iterator it2 = TQ.begin(); it2 != TQ.end(); ++it2) {
            if ((*it2)->getNextDeadline() == (int)cycle && (*it2)->getRemainingExecutionTime() > 0) {
                cout << " Task \"" << (*it2)->getName() << "\" missed it's Deadline at Cycle " << cycle << " !!!!"
                     << endl;
            }
        }
        // Update the task queue since new tasks might have been released
        updateTaskQueue(TQ, scenario, cycle);

        // ****************************************************************
        // * Implementation of DM scheduler
        // ****************************************************************

        // Only if there are tasks in the queue, we have something to do
        if (!TQ.empty()) {  // Sort tasks according to their remaining execution times
            TQ.sort(sortByRelativeDeadline);

            // Get first task T
            list<Task*>::iterator it = TQ.begin();
            Task* T = *it;

            // Schedule task in this cycle
            Schedule* S = taskSchedules[T];
            S->active(cycle);

            // Get remaining execution time of task T
            unsigned int d = T->getRemainingExecutionTime();
            if (d == 1) {  // The task has been totaly executed and can be removed from the task queue
                TQ.erase(it);
            } else {  // Decrement the remaining execution time by one cycle
                T->setRemainingExecutionTime(d - 1);
            }
        }
    }
    return taskSchedules;
}
